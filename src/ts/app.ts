import {Validators} from "./validators";

interface Config {
    slide: string,
    prev: string,
    next: string
    parentBlock?: string
    animationDuration?: number,
    showTime?: number
}

enum TypeEffect{
    NEXT = 'next',
    PREV = 'prev'
}

class Slider{

    /**
     * Список слайдов
     */
    private slides: NodeListOf<Element>;

    /**
     * Кнопка предыдущего слайда
     */
    private prev: Element;

    /**
     * Кнопка следующего слайда
     */
    private next: Element;

    /**
     * Блок слайдера
     */
    private parentBlock: Element;

    /**
     * Номер текущего слайда
     */
    private currentSlide: number;

    private slideInterval: number;

    /**
     * Начальные координаты точки touch
     */
    private startX: number;

    /**
     * Конфигурации слайдера по умолчанию
     * @type {{parentBlock: string; slide: string; prev: string; next: string; animationDuration: number; showTime: number}}
     */
    private static defaultConfig = {
        parentBlock: '.slides',
        slide: '.slide',
        prev: '.previous',
        next: '.next',
        animationDuration: 2,
        showTime: 2000
    };

    constructor(public config: Config){
        this.config = {
            ...Slider.defaultConfig,
            ...this.config
        };
        this.validateElement([this.config.parentBlock, this.config.slide, this.config.prev, this.config.next]);
        this.validateTime([this.config.animationDuration, this.config.showTime]);
        this.onInit();
    }

    private onInit(): void {
        this.slides = this.getAllSelector(this.config.slide);
        this.prev = this.getSelector(this.config.prev);
        this.next = this.getSelector(this.config.next);
        this.parentBlock = this.getSelector(this.config.parentBlock);
        this.getCurrentSlide();
        this.addTouchListener();
        this.addClickListener();
        this.slides.forEach((item: HTMLElement)=> {
            item.style.animationDuration = this.config.animationDuration + 's';
        });

        this.slideInterval = this.interval();
    }

    /**
     * Метод установки слушателя события touchstart
     */
    private addTouchListener(): void {
        if ('ontouchstart' in document.documentElement) {
            this.parentBlock.addEventListener('touchstart', (e: TouchEventInit) =>{
                this.touchStart(e);
            });
        }
    }

    /**
     * Метод установки слушателей события click
     */
    private addClickListener(): void {
        this.prev.addEventListener('click', () => {
            this.prevSlide();
        });

        this.next.addEventListener('click', () => {
            this.nextSlide();
        });
    }

    /**
     * Проверка конфигураций элементов слайдера
     * @param {string[]} arr
     */
    private validateElement(arr: string[]): void {
        arr.forEach((item: string) => {
            Validators.isDomElement(item);
        })
    }

    /**
     * Проверка конфигураций таймеров слайдера
     * @param {number[]} arr
     */
    private validateTime(arr: number[]): void {
        arr.forEach((item: number) => {
            Validators.isNumber(item);
        })
    }

    /**
     * Устанавливаем номер текущего слайда
     */
    private getCurrentSlide(): void {
        this.currentSlide = this.slides.length - 1;
        if(localStorage.getItem('currentSlide') !== null){
            this.currentSlide = Number(localStorage.getItem('currentSlide'));
            this.goToSlide(this.currentSlide, TypeEffect.NEXT);
        }
    }

    /**
     * Получаем список елементов
     * @param {string} name
     * @returns {NodeListOf<Element>}
     */
    private getAllSelector(name: string): NodeListOf<Element> {
        return document.querySelectorAll(name)
    }

    /**
     * Получаем елемент
     * @param {string} name
     * @returns {Element}
     */
    private getSelector(name: string): Element {
        return document.querySelector(name)
    }

    /**
     * Устанавливаем интервал слайдера
     * @returns {number}
     */
    private interval(): number {
        return setInterval( () => {
            this.nextSlide()
        }, this.config.showTime);
    }

    /**
     * Метод смены слада
     * @param {number} nextNum
     * @param {string} outSide
     * @param {string} inSide
     * @param {number} index
     */
    private sideSlide(nextNum: number, outSide: string, inSide: string, index?: number) {
        let nextIndex = nextNum % this.slides.length;
        this.slides[nextIndex].classList.remove(`${outSide}-out`);
        this.slides[this.currentSlide].className = `slide ${outSide}-out`;
        this.currentSlide = (index + this.slides.length) % this.slides.length;
        this.slides[this.currentSlide].className = `slide ${inSide}-in`;
        localStorage.setItem('currentSlide', this.currentSlide.toString());
    }

    /**
     * Определяем наплавления слайдела, номер предыдущего и следующего слайда
     * @param {number} index
     * @param {string} type
     */
    private goToSlide(index: number, type: string): void {
        if(type === TypeEffect.PREV){
            let prevIndex = this.currentSlide + 1;
            let nextIndex = index + this.slides.length;
            this.sideSlide(prevIndex, "right", "left", nextIndex);

        }else{
            let prevIndex = (this.currentSlide - 1 + this.slides.length);
            let nextIndex = this.currentSlide + 1;
            this.sideSlide(prevIndex, "left", "right", nextIndex);
        }
    }

    /**
     * Переходим на следующий слайд
     */
    private nextSlide(): void {
        clearInterval(this.slideInterval);
        this.goToSlide(this.currentSlide + 1, TypeEffect.NEXT);
        this.slideInterval = this.interval();
    };

    /**
     * Переходим на предыдущий слайд
     */
    private prevSlide(): void {
        clearInterval(this.slideInterval);
        this.goToSlide(this.currentSlide - 1, TypeEffect.PREV);
        this.slideInterval = this.interval();
    };

    /**
     * Оперделяем начальную точку touch
     * привязываем слушатель на событие touchend
     * @param {TouchEventInit} e
     */
    private touchStart(e: TouchEventInit): void {
        clearInterval(this.slideInterval);
        const [touchObj] = e.changedTouches;
        this.startX = touchObj.pageX;
        this.parentBlock.addEventListener('touchend', (e: TouchEventInit) =>{
            this.touchEnd(e);
        }, {once: true});
    };

    /**
     * Определяем координыты заерешения контакта с экраном
     * Вызываем метод следыющего/предыдущего слайдв
     * @param {TouchEventInit} e
     */
    private touchEnd(e: TouchEventInit): void {
        const [touchObj] = e.changedTouches;
        const dist = touchObj.pageX - this.startX;
        (dist > this.startX) ? this.prevSlide() : this.nextSlide();

    };
}

new Slider({
    parentBlock: '.slides',
    slide: '.slide',
    prev: '.previous',
    next: '.next',
    animationDuration: 2,
    showTime: 2000
});