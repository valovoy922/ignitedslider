const WRONG_CLASS_NAME = "wrong config name class";
const WRONG_TIME = "wrong config time";

export class Validators {

    /**
     * Проверка конфигураций таймера на число
     * @param {number} n
     */
    static isNumber(n: number): void {
        if(Number.isFinite(n) && n < 0){
            throw new Error(WRONG_TIME);
        }

    }

    /**
     * проверка конфигураций елементов слайдера на наличие в DOM делеве
     * @param {string} name
     */
    static isDomElement(name: string): void{
        if(document.querySelector(name) === null){
            throw new Error(WRONG_CLASS_NAME);
        }
    }
}